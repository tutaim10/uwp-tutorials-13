﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using WeaTherJSON1_12_17_2018.Models;
using System.Diagnostics;
using System.Text.RegularExpressions;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WeaTherJSON1_12_17_2018
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ObservableCollection<WeatherJSON> WeatherEachHours;
        ObservableCollection<DailyForecast> WeatherEachDays;
        public MainPage()
        {
            this.InitializeComponent();
            WeatherEachHours = new ObservableCollection<WeatherJSON>();
            InitJOSON();
            WeatherEachDays = new ObservableCollection<DailyForecast>();
            IntiEachDdaysJSON();
        }
        private async void InitJOSON()
        {
            var url = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/353412?" +
                "apikey=tbFOLXfZmAxAexEYOmXhcxnbZBDjQBSh&amp;language=vi-vn&amp;metric=true";
            var list = await WeatherJSON.GetJSON(url) as List<WeatherJSON>;
            Debug.WriteLine("Count:" + list.Count);

            list.ForEach(it =>
            {
                var math = Regex.Matches(it.DateTime, "T(?<time>\\d+")[0].Groups["time"].Value;
                if (int.Parse(math) > 12)
                {
                    math = (int.Parse(math) - 12) + "PM";
                }
                else
                {
                    math += "PM";
                }
                it.DateTime = math;
                it.Temperature.Valeu += "\u00B0";
                it.WeatherIcon = string.Format("http://vortex.accuweather.com/adc2010/image/slate/icons/{0}.svg",
                    it.WeatherIcon);
                WeatherEachHours.Add(it);
            });
            WeatherDescriptionTextBlock.Text = list[0].IconPhrase;
            WeatherTemperatureTextBloc.Text = list[0].Temperature.Valeu;
        }
        private async void IntiEachDdaysJSON()
        {
            var urlFiveDay = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/353412?" +
                "apikey=tbFOLXfZmAxAexEYOmXhcxnbZBDjQBSh&amp;language=vi-vn&amp;metric=true";
            var obj = await WeatherEachDay.GetWeatherEach(urlFiveDay) as WeatherEachDay;
            obj.DailyForecasts.ForEach(it =>
            {
                var matchs = Regex.Matches(it.Date, "\\d+");
                var date = new DateTime(int.Parse(matchs[0].Value), int.Parse(matchs[1].Value), int.Parse(matchs[2].Value));
                it.Date = date.DayOfWeek.ToString();
                it.Day.Icon = string.Format("https://vortex.accuweather.com/adc2010/images.slate/icons/{0}.svg",
                    it.Day.Icon);
                Debug.WriteLine("Binh: " + it.Date);
                WeatherEachDays.Add(it);
            });
            Today.Text = WeatherEachDays[0].Date + " Today";
            Maxtemperature.Text = WeatherEachDays[0].Temperature1.maximum.Valeu + "";
            MinTemperature.Text = WeatherEachDays[0].Temperature1.Minimum.Valeu + "";
            WeatherEachDays.RemoveAt(0);
        }
    }
}

